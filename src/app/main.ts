import {bootstrap} from '@angular/platform-browser-dynamic';
import {enableProdMode} from '@angular/core';
import {disableDeprecatedForms, provideForms} from '@angular/forms';
import {AppComponent} from './app.component';
import {APP_ROUTER_PROVIDERS} from './app.routes';

import {AuthService} from './services/authService';
import {Helpers} from './components/helpers/helpers';
import {SocketShared} from './shared/socket.shared';
import {SpinnerService} from './services/spinnerService';

declare var ENV: string;

if (ENV === 'production') {
    enableProdMode();
}

bootstrap(AppComponent, [
    disableDeprecatedForms(),
    provideForms(),
    APP_ROUTER_PROVIDERS,
    SocketShared,
    Helpers,
    SpinnerService,
    AuthService
]);
