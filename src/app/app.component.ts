import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

import { SpinnerComponent } from './components/helpers/spinner.component';

@Component({
  selector: 'epl-main-app',
  template: '<epl-spinner></epl-spinner><router-outlet></router-outlet>',
  directives: [ROUTER_DIRECTIVES, SpinnerComponent]
})

export class AppComponent {


}
