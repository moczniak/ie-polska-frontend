import {Injectable} from '@angular/core';

declare var socketCluster: any;

@Injectable()
export class SocketShared {

  socket: any;
  socketID: string;
  // '185.25.151.93'

  constructor() {
    let options = {
        hostname: 'localhost',
        port: 5800
    };
    this.socket = socketCluster.connect(options);
    this.waitForID();
  }


  waitForID() {
    let interval = setInterval(() => {
      if (this.socket.id !== null ) {
        this.socketID = this.socket.id;
        clearInterval(interval);
      }
    });
  }

}
