import { provideRouter, RouterConfig } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { gameRoutes } from './components/inGame/game.routes';

const routes: RouterConfig = [
  {
    path: '',
    component: HomeComponent

  },
  {
    path: 'activate/:token',
    component: HomeComponent
  },
  {
    path: 'remindMe/:remindMeToken',
    component: HomeComponent
  },
  ...gameRoutes


];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
