import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

import { AuthService } from './../../services/authService';
import { ErrorService } from './../../services/errorService';

import { MiniChatComponent } from './headers/miniChat.component';
import { ShortComponent } from './headers/short.component';
import { NotifyComponent } from './headers/notify.component';
import { ErrorComponent } from './headers/error.component';

@Component({
  selector: 'epl-game',
  templateUrl: 'app/components/inGame/game.template.html',
  directives: [ROUTER_DIRECTIVES, MiniChatComponent, ShortComponent, NotifyComponent, ErrorComponent],
  providers: [ErrorService]
})

export class GameComponent {

  constructor(private _authService: AuthService) {
    this._authService.isLoged$
            .subscribe(
              status => {
                if (status === false) {
                  console.log('trzeba wylogowac!');
                }
              }
            );
  }


}
