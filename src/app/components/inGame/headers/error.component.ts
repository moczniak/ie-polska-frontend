import { Component, OnInit, OnDestroy } from '@angular/core';

import { ErrorService } from './../../../services/errorService';

@Component({
  selector: 'epl-error',
  template: `
            <div [hidden]="!show" class="alert alert-danger">
                {{error}}
             </div>
  `
})


export class ErrorComponent implements OnInit, OnDestroy {

  show: boolean = false;
  error: string;

  private _sub: any;

  constructor(private _errorService: ErrorService) { }

  ngOnInit() {
  this._sub = this._errorService.error$.subscribe(
      value => {
        this.error = value;
        this.startShowing();
      }
    );
  }

  ngOnDestroy() {
    this._sub.unsubscribe();
  }


  startShowing() {
      this.show = true;
      setTimeout(() => {
        this.show = false;
      }, 5000);
  }

}
