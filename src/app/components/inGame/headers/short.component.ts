import { Component, OnInit, OnDestroy } from '@angular/core';

import { UserService } from './../../../services/userService';
import { SecToStr } from './../../../pipes/secToStr.pipe';

@Component({
  selector: 'epl-short',
  templateUrl: 'app/components/inGame/headers/short.template.html',
  providers: [UserService],
  pipes: [SecToStr]
})


export class ShortComponent implements OnInit, OnDestroy {

  private _subs = [];

  shortData: any;

  private workInterval: any;
  private energyInterval: any;

  constructor(private _userService: UserService) { }

  ngOnInit() {
    this.getDetails();
    this._userService.subscribeUserDetail();
    this._subs[this._subs.length] = this._userService.userDetail$
              .subscribe(
                value => {
                  clearInterval(this.workInterval);
                  clearInterval(this.energyInterval);
                  this.shortData = value;
                  if (this.shortData.workTimeLeft > 0) {
                    this.workCounter();
                  }
                  if (this.shortData.energyTimeLeft > 0) {
                    this.energyCounter();
                  }
                }
              );
  }

  getDetails() {
    this._subs[this._subs.length] = this._userService.getUserDetails()
              .subscribe(
                value => {
                  clearInterval(this.workInterval);
                  clearInterval(this.energyInterval);
                  this.shortData = value.data;
                  if (this.shortData.workTimeLeft > 0) {
                    this.workCounter();
                  }
                  if (this.shortData.energyTimeLeft > 0) {
                    this.energyCounter();
                  }
                }
              );
  }

  workCounter() {
    this.workInterval = setInterval(() => {
      if (this.shortData.workTimeLeft <= 0) {
        clearInterval(this.workInterval);
        this.getDetails();
      }
      this.shortData.workTimeLeft--;
    }, 1000);
  }

  energyCounter() {
    this.energyInterval = setInterval(() => {
      if (this.shortData.energyTimeLeft <= 0) {
        clearInterval(this.energyInterval);
        this.getDetails();
      }
      this.shortData.energyTimeLeft--;
    }, 1000);
  }


  ngOnDestroy() {
    this._subs.map(sub => {
      sub.unsubscribe();
    });
  }

}
