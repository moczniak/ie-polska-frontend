import { Component, OnInit } from '@angular/core';

import { ChatService } from './../../../services/chatService';
import { TimeStrPipe } from './../../../pipes/timeStr.pipe';
import { Chat } from './../../../class/chat';

@Component({
  selector: 'epl-miniChat',
  templateUrl: 'app/components/inGame/headers/miniChat.template.html',
  providers: [ChatService],
  pipes: [TimeStrPipe]
})

export class MiniChatComponent implements OnInit {

  message: string;

  messages: Chat[];

  constructor(private _chatService: ChatService) {

  }

  ngOnInit() {
    this.subscribeMainChat();
    this.getMessages(3);
  }


  getMessages(limit: number) {
    this._chatService.getMessages(limit)
                .subscribe(
                  newMessages => {
                    this.messages = newMessages;
                  },
                  error => console.log(error)
                );
  }

  subscribeMainChat() {
    this._chatService.newMainChat$
                .subscribe(
                  newMessage => {
                    this.messages.push(newMessage);
                    this.messages.shift();
                  }
                );
  }

  sendMessage() {
    if (this.message.replace(/\s/g, '').length >= 2) {
      this._chatService.sendMessage(this.message)
                .subscribe(
                  success => {
                    this.message = '';
                  }
                );
    }
  }

}
