import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';

import { UserService } from './../../../services/userService';

import { PerfectScroll } from './../../../libClass/perfectScroll';
import { ScrollBottom } from './../../../libClass/scrollBottom';

@Component({
  selector: 'epl-currentTask',
  templateUrl: 'app/components/inGame/headers/notify.template.html',
  providers: [UserService],
  directives: [ScrollBottom]
})


export class NotifyComponent implements OnInit, AfterViewInit, OnDestroy {

  private _scroll: any;
  private _subs = [];

  notify = [];

  constructor(private _userService: UserService) {

  }

  ngOnInit() {
    this._userService.subscribeUserNotify();
    this._subs[this._subs.length] = this._userService.userNotify$
              .subscribe(
                value => {
                  this.notify.push(value);
                }
              );
  }

  ngAfterViewInit() {
    this._scroll = new PerfectScroll();
    this._scroll.setContainerById('notifyScroll');
    this._scroll.init();
  }


  ngOnDestroy() {
    this._scroll.destroy();
  }


}
