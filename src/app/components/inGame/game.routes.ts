import { RouterConfig } from '@angular/router';

import { GameComponent }  from './game.component';
import { LifeComponent } from './life/life.component';
import { WorkComponent } from './work/work.component';



export const gameRoutes: RouterConfig = [
  {
    path: 'game',  component: GameComponent,
    children: [
        { path: 'life',     component: LifeComponent },
        { path: 'work', component: WorkComponent }
      ]
  }


];
