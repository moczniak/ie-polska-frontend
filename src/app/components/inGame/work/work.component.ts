import { Component, AfterViewInit, OnInit } from '@angular/core';

import { PerfectScroll } from './../../../libClass/perfectScroll';
import { CHART_DIRECTIVES } from 'ng2-charts/ng2-charts';

import { WorkService } from './../../../services/workService';
import { ErrorService } from './../../../services/errorService';

@Component({
  selector: 'epl-work',
  templateUrl: 'app/components/inGame/work/work.template.html',
  providers: [ WorkService ],
  directives: [CHART_DIRECTIVES]
})



export class WorkComponent implements AfterViewInit, OnInit {

  private _stateJobOffers: any;

  weekChartData: Array<any> = [
    {data: [], label: 'Obecny tydzien'},
    {data: [], label: 'Poprzedni tydzien'}
  ];
  weekChartLabels: Array<any> = ['Poniedzialek', 'Wtorek', 'Sroda', 'Czwartek', 'Piatek', 'Sobota', 'Niedziela'];

  monthChartData: Array<any> = [
    {data: [0, 0, 0, 0, 12, 0, 0, 9, 0, 0, 0, 10], label: 'Obecny miesiąc'},
    {data: [16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Poprzedni miesiąc'}
  ];
  monthChartLabels: Array<any> = ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec',
                                  'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'];

  chartOptions: any = {
    animation: false,
    responsive: false
  };
  chartColours: Array<any> = [
    { // green
      backgroundColor: 'rgba(59, 214, 77,0.2)',
      borderColor: 'rgba(59, 214, 77,1)',
      pointBackgroundColor: 'rgba(59, 214, 77,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(59, 214, 77,0.8)'
    },
    { // blue
      backgroundColor: 'rgba(59, 146, 214,0.2)',
      borderColor: 'rgba(59, 146, 214,1)',
      pointBackgroundColor: 'rgba(59, 146, 214,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(59, 146, 214,1)'
    }
  ];
  chartLegend: boolean = true;
  chartType: string = 'line';


  constructor(private _workService: WorkService, private _errorService: ErrorService) { }

  ngOnInit() {
    this.loadChartWeekData();
    // this.loadChartMonthData();
  }

  ngAfterViewInit() {
    this._stateJobOffers = new PerfectScroll();
    this._stateJobOffers.setContainerById('stateJobOffers');
    this._stateJobOffers.init();
  }

  goToWork() {
    this._workService.goToWork()
              .subscribe(
                success => {
                  console.log(success);
                  let tmp = this.weekChartData.slice(0);
                  tmp[0].data[success.today] += success.earned;
                  this.weekChartData = tmp;
                 },
                error => this._errorService.setError(error)
              );
  }

  loadChartWeekData() {
    this._workService.getChartWeekData('week')
              .subscribe(
                success => {
                  let tmp = this.weekChartData.slice(0);
                  tmp[0].data = success.data.thisWeek;
                  tmp[1].data = success.data.lastWeek;
                  this.weekChartData = tmp;
                }
              );
  }

  loadChartMonthData() {
    this._workService.getChartWeekData('month')
              .subscribe(
                success => {
                    console.log(success);
                }
              );
  }


}
