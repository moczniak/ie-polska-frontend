import {Component} from '@angular/core';

import {SpinnerService} from './../../services/spinnerService';

@Component({
  selector: 'epl-spinner',
  template: `<div class="overlay" [hidden]="!show">
                <i class="loadingSpinner fa fa-cog fa-spin fa-3x fa-fw"></i>
             </div>`
})

export class SpinnerComponent {

  private _timeout: any;
  show: boolean = false;

  constructor(private _spinnerService: SpinnerService) {
    this._spinnerService.isSpinning$
            .subscribe(value => {
              value === true ? this.startShowing() : this.stopShowing();
            });
  }


  startShowing() {
    this._timeout = setTimeout(() => {
      this.show = true;
    }, 500);
  }

  stopShowing() {
    clearTimeout(this._timeout);
    this.show = false;
  }
}
