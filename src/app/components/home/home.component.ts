import {Component, OnInit, OnDestroy} from '@angular/core';
import { ActivatedRoute, ROUTER_DIRECTIVES } from '@angular/router';

import {RegisterForm} from './../../class/forms/registerForm';
import {LoginForm} from './../../class/forms/loginForm';
import {RemindMeForm} from './../../class/forms/remindMeForm';

import {ErrorPopupComponent} from './../helpers/errorPopup.component';
import {SuccessPopupComponent} from './../helpers/successPopup.component';
import {Helpers} from './../helpers/helpers';

import {HomeService} from './../../services/homeService';
import {AuthService} from './../../services/authService';
import {LocalStorage} from './../../services/localStorage';

@Component({
  selector: 'epl-home',
  templateUrl: 'app/components/home/home.template.html',
  directives: [ErrorPopupComponent, SuccessPopupComponent, ROUTER_DIRECTIVES],
  providers: [HomeService, LocalStorage]
})


export class HomeComponent implements OnInit, OnDestroy {

  registerForm = new RegisterForm('', '', '', '', false);
  loginForm = new LoginForm('', '', false);
  remindMeForm = new RemindMeForm('', '', '');

  registerFormErr = '';
  registerFormErrShow: number = 0;

  registerFormSuccess = '';
  registerFormSuccessShow: number = 0;

  loginFormErr = '';
  loginFormErrShow: number = 0;

  loginFormSuccess = '';
  loginFormSuccessShow: number = 0;

  isLoged: boolean = false;
  forgetStep1: boolean = false;
  forgetStep2: boolean = false;
  forgetToken: string;

  autoLoginPerform: boolean = false;

  private sub = [];

  constructor(private _helpers: Helpers, private _homeService: HomeService, private _route: ActivatedRoute,
              private _authService: AuthService, private _localStorage: LocalStorage) {
    this._authService.checkIfAuth();
    this._authService.isLoged$
            .subscribe(
              status => {
                this.isLoged = status;
                if (status === false) {
                  this.autoLogin();
                }
              }
            );
  }

  ngOnInit() {
    this.sub[this.sub.length] =  this._route.params.subscribe(params => {
      if (typeof params['token'] !== 'undefined') {
        this._homeService.activateUser(params['token'])
                    .subscribe(
                      result => this.showLogSuccess(result.message),
                      error => this.showLogError(error)
                    );
      }
      if (typeof params['remindMeToken'] !== 'undefined') {
        this.forgetToken = params['remindMeToken'];
        this.forgetStep1 = true;
        this.forgetStep2 = true;
      }
    });
  }

  ngOnDestroy() {
    this.sub.map((sub) => {
      sub.unsubscribe();
    });
  }

  forgetPassword(e) {
    e.preventDefault();
    this.forgetStep1 = true;
  }
  backToLogin(e) {
    e.preventDefault();
    this.forgetStep1 = false;
  }


  changePassword() {
    if (this.remindMeForm.password === this.remindMeForm.passwordConfirm) {
    this.sub[this.sub.length] =
            this._homeService.changePassword(this.remindMeForm.password, this.remindMeForm.passwordConfirm, this.forgetToken)
                      .subscribe(
                        success => {
                          this.showLogSuccess(success.message);
                          this.remindMeForm = new RemindMeForm('', '', '');
                        },
                        error => this.showLogError(error)
                      );
    }else {
      this.showLogError('Hasła muszą się zgadzać');
    }

  }
  remindMe() {
    this.sub[this.sub.length] = this._homeService.remindMe(this.remindMeForm.email)
                    .subscribe(
                      success => this.showLogSuccess(success.message),
                      error => this.showLogError(error)
                    );
  }

  logout() {
    this.sub[this.sub.length] = this._homeService.logoutUser()
                    .subscribe(success => {
                        this.isLoged = false;
                        this._localStorage.removeKey('autoLoginToken');
                      }
                    );
  }


  autoLogin() {
    if (this.isLoged === false) {
      if (this.autoLoginPerform === false) {
        this.autoLoginPerform = true;
        let token = this._localStorage.getKey('autoLoginToken');
        if (typeof token !== 'undefined') {
        this.sub[this.sub.length] = this._homeService.autoLoginUser(token)
                          .subscribe(
                            data => {
                              this.isLoged = true;
                            },
                            error => {
                              this.showLogError(error);
                              this._localStorage.removeKey('autoLoginToken');
                            }
                          );
        }
      }
    }
  }

  loginSubmit() {
    this.sub[this.sub.length] = this._homeService.loginUser(this.loginForm)
                    .subscribe(
                      data => {
                        this.isLoged = true;
                        if (this.loginForm.rememberme === true) {
                          this._localStorage.setKey('autoLoginToken', data.autoLoginToken);
                          this._localStorage.setExpiration();
                        }
                        this.loginForm = new LoginForm('', '', false);
                      },
                      error => this.showLogError(error)
                    );
  }

  registerSubmit() {
    if (this.registerForm.password === this.registerForm.password2) {
      if (this.registerForm.rules === true) {

        this.sub[this.sub.length] = this._homeService.registerUser(this.registerForm)
                    .subscribe(
                      data => {
                        this.showRegSuccess(data.message);
                        this.registerForm = new RegisterForm('', '', '', '', false);
                      },
                      error => this.showRegError(error)
                    );

      }else { this.showRegError('Zaakceptuj zasady'); }
    }else { this.showRegError('Hasla muszą się zgadzać'); }
  }

  showRegError(txt: string) {
    this.registerFormErr = txt;
    this.registerFormErrShow++;
  }

  showRegSuccess(txt: string) {
    this.registerFormSuccess = txt;
    this.registerFormSuccessShow++;
  }

  showLogError(txt: string) {
    this.loginFormErr = txt;
    this.loginFormErrShow++;
  }

  showLogSuccess(txt: string) {
    this.loginFormSuccess = txt;
    this.loginFormSuccessShow++;
  }





}
