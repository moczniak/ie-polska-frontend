declare var PerfectScrollbar: any;


export class PerfectScroll {

  private _perfectScroll: any;

  private _container: any;

  constructor() {
    this._perfectScroll = PerfectScrollbar;
  }

  setContainerById(selector: string) {
    this._container = document.getElementById(selector);
  }

  init() {
    this._perfectScroll.initialize(this._container);
  }

  update() {
    this._perfectScroll.update(this._container);
  }

  destroy() {
    this._perfectScroll.destroy(this._container);
  }

  destructor() {
    this._perfectScroll.destroy(this._container);
  }

}
