import { Directive, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';



@Directive({ selector: '[scrollBottom]' })
export class ScrollBottom implements AfterViewInit, OnDestroy {

  private _observer: any;

  constructor(private _element: ElementRef) { }

  ngAfterViewInit() {
    this._element.nativeElement.scrollTop = this._element.nativeElement.scrollHeight;

    this._observer = new MutationObserver(mutation => {
      this._element.nativeElement.scrollTop = this._element.nativeElement.scrollHeight;
    });

    const config = { attributes: true, childList: true, characterData: true };
    this._observer.observe(this._element.nativeElement, config);
  }

  ngOnDestroy() {
    this._observer.disconnect();
  }

}
