import { Injectable } from '@angular/core';
import { SocketShared } from './../shared/socket.shared';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { AuthService } from './../services/authService';

@Injectable()
export class UserService {

    private _socket: any;

    private _userDetailChanel: any;
    private _userDetail = new Subject<any>();
    userDetail$ = this._userDetail.asObservable();

    private _userNotifyChanel: any;
    private _userNotify = new Subject<any>();
    userNotify$ = this._userNotify.asObservable();


    constructor(private _socketShared: SocketShared, private _authService: AuthService ) {
      this._socket = this._socketShared.socket;
    }

    subscribeUserDetail() {
      setTimeout(() => {
        this._userDetailChanel = this._socket.subscribe('userDetail_' + this._socketShared.socketID);
        this.watchUserDetail();
      }, 1000);
    }

    watchUserDetail() {
      this._userDetailChanel.watch(result => {
        this._userDetail.next(result);
      });
    }


    subscribeUserNotify() {
      setTimeout(() => {
        this._userNotifyChanel = this._socket.subscribe('userNotify_' + this._socketShared.socketID);
        this.watchUserNotify();
      }, 1000);
    }

    watchUserNotify() {
      this._userNotifyChanel.watch(result => {
        this._userNotify.next(result);
      });
    }


    getUserDetails() {
      return Observable.create((observer) => {
        this._socket.emit('getUserDetails:Auth', {}, (respond) => {
          this.parseData(respond).then((result: any) => {
            observer.next(result);
          }).catch((error: any) => {
            observer.error(error);
          });
        });
      });
    }



    parseData(data: any) {
      return new Promise((resolve, reject) => {
        let json = JSON.parse(data);
        if (json.status === 'ok') {
          resolve(json);
        }else if (json.status === 'unAuth') {
          this._authService.checkIfAuth();
        }else {
          reject(json.error);
        }
      });
    }
}
