import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class ErrorService {

  private _error = new Subject<string>();
  error$ = this._error.asObservable();

  setError(err: string) {
    this._error.next(err);
  }

}
