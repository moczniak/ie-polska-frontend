import { Injectable } from '@angular/core';
import { SocketShared } from './../shared/socket.shared';
import { Observable } from 'rxjs/Observable';

import { AuthService } from './../services/authService';


@Injectable()
export class WorkService {

  private _socket: any;

  constructor(private _socketShared: SocketShared, private _authService: AuthService ) {
    this._socket = this._socketShared.socket;
  }


  getChartWeekData(period: string) {
    return Observable.create((observer) => {
      this._socket.emit('getChartData:Auth', {period}, (respond) => {
        this.parseData(respond).then((result: any) => {
          observer.next(result);
        }).catch((error: any) => {
          observer.error(error);
        });
      });
    });
  }


  goToWork() {
    return Observable.create((observer) => {
      this._socket.emit('goToWork:Auth', {}, (respond) => {
        this.parseData(respond).then((result: any) => {
          observer.next(result);
        }).catch((error: any) => {
          observer.error(error);
        });
      });
    });
  }



  parseData(data: any) {
    return new Promise((resolve, reject) => {
      let json = JSON.parse(data);
      if (json.status === 'ok') {
        resolve(json);
      }else if (json.status === 'unAuth') {
        this._authService.checkIfAuth();
      }else {
        reject(json.error);
      }
    });
  }

}
