import { Injectable } from '@angular/core';
import { SocketShared } from './../shared/socket.shared';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AuthService {

  private _loged = new Subject<boolean>();
  isLoged$ = this._loged.asObservable();

  private _socket: any;

  constructor(private _socketShared: SocketShared) {
    this._socket = this._socketShared.socket;
    this.checkIfAuth();
    this._socket.on('connect', (status) => {
      if (status.isAuthenticated === true) {
        this._loged.next(true);
      }else {
        this._loged.next(false);
      }
    });
  }


  checkIfAuth() {
    this._socket.emit('checkIfAuth', {}, (respond) => {
      let json = JSON.parse(respond);
      this._loged.next(json.status);
    });
  }






}
