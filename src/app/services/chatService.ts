import { Injectable } from '@angular/core';
import { SocketShared } from './../shared/socket.shared';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { Chat } from './../class/chat';

import { AuthService } from './../services/authService';


@Injectable()
export class ChatService {

  private _socket: any;
  private _chatChanel: any;

  private _newMainChat = new Subject<any>();
  newMainChat$ = this._newMainChat.asObservable();


  constructor(private _socketShared: SocketShared, private _authService: AuthService) {
    this._socket = this._socketShared.socket;
    this._chatChanel = this._socket.subscribe('mainChat');
    this.watchMainChat();
  }

  watchMainChat() {
    this._chatChanel.watch((chat) => {
      this._newMainChat.next(chat);
    });
  }

  sendMessage(message: string) {
    return Observable.create((observer) => {
      this._socket.emit('chatSendMessage:Auth', message, (respond) => {
        this.parseData(respond).then((result: Chat) => {
          observer.next(result);
        }).catch((error: any) => {
          observer.error(error);
        });
      });
    });
  }

  getMessages(limit: number) {
    return Observable.create((observer) => {
      this._socket.emit('getMainChatMessages', limit, (respond) => {
        this.parseMessages(respond).then((result: Chat) => {
          observer.next(result);
        }).catch((error: any) => {
          observer.error(error);
        });
      });
    });
  }



  parseMessages(data: any) {
    return new Promise((resolve, reject) => {
      let json = JSON.parse(data);
      if (json.status === 'ok') {
        resolve(json.data);
      }else {
        reject(json.error);
      }
    });
  }

  parseData(data: any) {
    return new Promise((resolve, reject) => {
      let json = JSON.parse(data);
      if (json.status === 'ok') {
        resolve(json);
      }else if (json.status === 'unAuth') {
        this._authService.checkIfAuth();
      }else {
        reject(json.error);
      }
    });
  }

}
