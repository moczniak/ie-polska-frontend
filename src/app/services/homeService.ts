import { Injectable } from '@angular/core';
import { SocketShared } from './../shared/socket.shared';
import { Observable } from 'rxjs/Observable';
import { SpinnerService } from './spinnerService';

import { RegisterForm } from './../class/forms/registerForm';
import { LoginForm } from './../class/forms/loginForm';

@Injectable()
export class HomeService {

  private _socket: any;

  constructor(private _socketShared: SocketShared, private _spinnerService: SpinnerService) {
    this._socket = this._socketShared.socket;
  }

  logoutUser() {
    return Observable.create((observer) => {
      this._spinnerService.showSpinning();
      this._socket.emit('logoutUser', {}, (respond) => {
        this._spinnerService.stopSpinning();
        this.parseData(respond).then((result: any) => {
          observer.next(result);
        }).catch((error: any) => {
          observer.error(error);
        });
      });
    });
  }

  changePassword(password: string, passwordConfirm: string, token: string) {
    return Observable.create((observer) => {
      this._spinnerService.showSpinning();
      this._socket.emit('changePassword', {password, passwordConfirm, token}, (respond) => {
        this._spinnerService.stopSpinning();
        this.parseData(respond).then((result: any) => {
          observer.next(result);
        }).catch((error: any) => {
          observer.error(error);
        });
      });
    });
  }

  remindMe(email: string) {
    return Observable.create((observer) => {
      this._spinnerService.showSpinning();
      this._socket.emit('remindMe', email, (respond) => {
        this._spinnerService.stopSpinning();
        this.parseData(respond).then((result: any) => {
          observer.next(result);
        }).catch((error: any) => {
          observer.error(error);
        });
      });
    });
  }

  autoLoginUser(token: string) {
    return Observable.create((observer) => {
      this._socket.emit('autoLoginUser', token, (respond) => {
        this.parseData(respond).then((result: any) => {
          observer.next(result);
        }).catch((error: any) => {
          observer.error(error);
        });
      });
    });
  }

  loginUser (data: LoginForm) {
    return Observable.create((observer) => {
      this._spinnerService.showSpinning();
      this._socket.emit('loginUser', data, (respond) => {
        this._spinnerService.stopSpinning();
        this.parseData(respond).then((result: any) => {
          observer.next(result);
        }).catch((error: any) => {
          observer.error(error);
        });
      });
    });
  }

  activateUser(token: string) {
    return Observable.create((observer) => {
      this._spinnerService.showSpinning();
      this._socket.emit('activateUser', token, (respond) => {
        this._spinnerService.stopSpinning();
        this.parseData(respond).then((result: any) => {
          observer.next(result);
        }).catch((error: any) => {
          observer.error(error);
        });
      });
    });
  }

  registerUser(data: RegisterForm) {
    return Observable.create((observer) => {
      this._spinnerService.showSpinning();
      this._socket.emit('registerUser', data, (respond) => {
        this._spinnerService.stopSpinning();
        this.parseData(respond).then((result: any) => {
          observer.next(result);
        }).catch((error: any) => {
          observer.error(error);
        });
      });
    });
  }

  parseData(data: any) {
    return new Promise((resolve, reject) => {
      let json = JSON.parse(data);
      if (json.status === 'ok') {
        resolve(json);
      }else {
        reject(json.error);
      }
    });
  }



}
