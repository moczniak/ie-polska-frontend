import { Pipe, PipeTransform } from '@angular/core';



@Pipe({name: 'timeStr'})
export class TimeStrPipe implements PipeTransform {

  timeConverter(UNIX_timestamp: number) {

    let a = new Date(UNIX_timestamp * 1000);
    let year: string = a.getFullYear().toString();
    let month: string = (a.getMonth() + 1).toString();
    let date: string = a.getDate().toString();
    let hour: string = a.getHours().toString();
    let min: string = a.getMinutes().toString();
    let sec: string = a.getSeconds().toString();


    if (parseInt(hour, 10) < 10) {
      hour = '0' + hour;
    }

    if (parseInt(min, 10) < 10) {
      min = '0' + min;
    }

    if (parseInt(sec, 10) < 10) {
      sec = '0' + sec;
    }

    if (parseInt(month, 10) < 10) {
      month = '0' + month;
    }

    if (parseInt(date, 10) < 10) {
      date = '0' + date;
    }



    return {
      Y: year,
      m: month,
      d: date,
      H: hour,
      i: min,
      s: sec
    };
  }

  convertToStr(timestamp)	{

    let date = this.timeConverter(timestamp);


    let d = new Date();
    d.setHours(0, 0, 0, 0);
    let today = d.getTime() / 1000;

    let yesterday = today - (24 * 60 * 60);
    let tommorow = today + (24 * 60 * 60);
    let before2 = yesterday - (24 * 60 * 60);
    let koniec: string;

    if (timestamp >= tommorow + (24 * 60 * 60)) {
      koniec = date.Y + '-' + date.m + '-' + date.d + ' ' + date.H + ':' + date.i + ':' + date.s;
    }else if (timestamp >= tommorow) {
      koniec = 'Jutro o ' + date.H + ':' + date.i + ':' + date.s;
    }else if (timestamp >= today) {
      koniec = 'Dzisiaj o ' + date.H + ':' + date.i + ':' + date.s;
    }else if (timestamp >= yesterday) {
      koniec = 'Wczoraj o ' + date.H + ':' + date.i + ':' + date.s;
    }else if (timestamp >= before2) {
      koniec = 'Przedwczoraj o ' + date.H + ':' + date.i + ':' + date.s;
    }else {
      koniec = date.Y + '-' + date.m + '-' + date.d + ' ' + date.H + ':' + date.i + ':' + date.s;
    }
    return koniec;
  }


  transform(value: number): string {
    return this.convertToStr(value);
  }
}
