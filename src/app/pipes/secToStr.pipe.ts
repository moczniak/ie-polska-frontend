import { Pipe, PipeTransform } from '@angular/core';



@Pipe({name: 'secToStr'})
export class SecToStr implements PipeTransform {

  convertToStr(seconds, format) {

    let day = Math.floor( seconds / 24 / 60 / 60 );
    let left = seconds - day * 24 * 60 * 60;
    let hour = Math.floor( left / 60 / 60 );

    let min = seconds / 60;
    min = Math.floor(min % 60);

    let sec = Math.floor(seconds % 60);

    let minStr = min.toString();
    let secStr = sec.toString();
    let hourStr = hour.toString();

    if (hour < 10) {
      hourStr = '0' + hourStr;
    }
    if (min < 10) {
      minStr = '0' + minStr;
    }
    if (sec < 10) {
      secStr = '0' + sec;
    }

    if (format === 'hour') {
      return hourStr + ':' + minStr + ':' + secStr;
    }else {
      return minStr + ':' + secStr;
    }

  }


  transform(value: number, arg: string): string {
    return this.convertToStr(value, arg);
  }
}
