export class RegisterForm {

  constructor(
    public login: string,
    public email: string,
    public password: string,
    public password2: string,
    public rules: boolean
  ) {  }

}
